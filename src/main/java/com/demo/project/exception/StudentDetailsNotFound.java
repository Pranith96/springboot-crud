package com.demo.project.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Student Details Not Found")
public class StudentDetailsNotFound extends RuntimeException {

	private static final long serialVersionUID = 1L;

	private String message;

	public StudentDetailsNotFound(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}
}
