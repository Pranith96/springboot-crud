package com.demo.project.service;

import com.demo.project.entity.Student;
import com.demo.project.exception.StudentDetailsNotFound;

import java.util.List;

public interface StudentService {
    Student create(Student student) throws Exception;

    List<Student> getStudents() throws Exception;

    Student getStudentDetails(int id) throws StudentDetailsNotFound;

    Student getStudentDetailsByName(String name) throws StudentDetailsNotFound;

    void deleteStudent(int id);

    Student updateStudent(Student student) throws StudentDetailsNotFound;

    void updateStudentName(int id, String name) throws Exception;
}
