package com.demo.project.service;

import com.demo.project.entity.Student;
import com.demo.project.exception.StudentDetailsNotFound;
import com.demo.project.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class StudentServiceImpl implements StudentService {

    @Autowired
    StudentRepository repository;

    public Student create(Student student) throws Exception {
        Student response = repository.save(student);
        if (response == null) {
            throw new Exception("Data not saved");
        }
        return response;
    }

    @Override
    public List<Student> getStudents() throws Exception {
        List<Student> response = repository.findAll();
        if (response.isEmpty() || response == null) {
            throw new Exception("Data not available");
        }
        return response;
    }

    @Override
    public Student getStudentDetails(int id) throws StudentDetailsNotFound {
        Optional<Student> student = repository.findById(id);
        if (!student.isPresent()) {
            throw new StudentDetailsNotFound("Student Id doesnt exists");
        }
        return student.get();
    }

    @Override
    public Student getStudentDetailsByName(String name) throws StudentDetailsNotFound {
        Optional<Student> student = repository.findByName(name);
        if (!student.isPresent()) {
            throw new StudentDetailsNotFound("Student Name doesnt exists");
        }
        return student.get();
    }

    public void deleteStudent(int id) {
        repository.deleteById(id);
    }

    @Override
    public Student updateStudent(Student student) throws StudentDetailsNotFound {
        Optional<Student> response = repository.findById(student.getId());
        if (!response.isPresent()) {
            throw new StudentDetailsNotFound("Student Id doesnt exists");
        }

        response.get().setId(student.getId());
        if (student.getName() != null) {
            response.get().setName(student.getName());
        }
        if (student.getAddress() != null) {
            response.get().setAddress(student.getAddress());
        }
        if (student.getStandard() != null) {
            response.get().setStandard(student.getStandard());
        }
//        if (student.getSchoolName() != null) {
//            response.get().setSchoolName(student.getSchoolName());
//        }

        Student updatedResponse = repository.save(response.get());
        return updatedResponse;
    }

    @Override
    public void updateStudentName(int id, String name) throws Exception {
        Optional<Student> response = repository.findById(id);
        if (!response.isPresent()) {
            throw new Exception("Data not found");
        }
         repository.updateStudentName(name,id);
    }
}
