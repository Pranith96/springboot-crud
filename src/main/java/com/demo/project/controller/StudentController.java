package com.demo.project.controller;

import com.demo.project.entity.Student;
import com.demo.project.exception.StudentDetailsNotFound;
import com.demo.project.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.ResultProcessor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class StudentController {

	@Autowired
	StudentService service;

	@PostMapping("/create/student")
	public ResponseEntity<Student> createStudent(@RequestBody Student student) throws Exception {
		Student studentResponse = service.create(student);
		return ResponseEntity.status(HttpStatus.CREATED).body(studentResponse);
	}

	@GetMapping("/get/student")
	public ResponseEntity<List<Student>> getAllStudents() throws Exception {
		List<Student> students = service.getStudents();
		return ResponseEntity.status(HttpStatus.OK).body(students);
	}

	@GetMapping("/get/student/{id}")
	public ResponseEntity<Student> getStudentById(@PathVariable("id") int id) throws StudentDetailsNotFound {
		Student studentResponse = service.getStudentDetails(id);
		return ResponseEntity.status(HttpStatus.OK).body(studentResponse);

	}

	@GetMapping("/get/student/name")
	public ResponseEntity<Student> getStudentByName(@RequestParam("name") String name) throws StudentDetailsNotFound {
		Student studentResponse = service.getStudentDetailsByName(name);
		return ResponseEntity.status(HttpStatus.OK).body(studentResponse);
	}

	@DeleteMapping("/delete/student/{id}")
	public ResponseEntity<String> deleteStudent(@PathVariable("id") int id) {
		service.deleteStudent(id);
		return ResponseEntity.status(HttpStatus.OK).body("student Deleted with Id" + id);
	}

	@PutMapping("/update/student")
	public ResponseEntity<Student> updateStudent(@RequestBody Student student) throws StudentDetailsNotFound {
		Student response = service.updateStudent(student);
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}

	@PutMapping("/update/student/{id}/{name}")
	public ResponseEntity<String> updateStudentName(@PathVariable("id") int id, @PathVariable("name") String name)
			throws Exception {
		service.updateStudentName(id, name);
		return ResponseEntity.status(HttpStatus.OK).body("updated student name");
	}
}
