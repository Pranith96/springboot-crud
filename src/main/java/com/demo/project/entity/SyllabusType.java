package com.demo.project.entity;

public enum SyllabusType {

	SSC("ssc"), ICSE("icse"), CBSE("cbse");

	String code;

	SyllabusType(String code) {
		this.code = code;
	}

	public String getCode() {
		return code;
	}
}
