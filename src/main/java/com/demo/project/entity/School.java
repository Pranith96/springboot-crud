package com.demo.project.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

@Entity
public class School implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer schoolId;
	private String schoolName;
	private String address;

	@Enumerated(EnumType.STRING)
	private SyllabusType SyllabusType;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
	@JoinColumn(name="student_id")
	private List<Student> student;

	public School(Integer schoolId, String schoolName, String address,
			com.demo.project.entity.SyllabusType syllabusType, List<Student> student) {
		this.schoolId = schoolId;
		this.schoolName = schoolName;
		this.address = address;
		SyllabusType = syllabusType;
		this.student = student;
	}

	public School() {
	}

	public Integer getSchoolId() {
		return schoolId;
	}

	public void setSchoolId(Integer schoolId) {
		this.schoolId = schoolId;
	}

	public String getSchoolName() {
		return schoolName;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public SyllabusType getSyllabusType() {
		return SyllabusType;
	}

	public void setSyllabusType(SyllabusType syllabusType) {
		SyllabusType = syllabusType;
	}

	public List<Student> getStudent() {
		return student;
	}

	public void setStudent(List<Student> student) {
		this.student = student;
	}

	@Override
	public String toString() {
		return "School [schoolId=" + schoolId + ", schoolName=" + schoolName + ", address=" + address
				+ ", SyllabusType=" + SyllabusType + ", student=" + student + "]";
	}

}
